FROM php:7.0-alpine
RUN docker-php-ext-install pcntl
COPY . /app
WORKDIR /app
CMD [ "php", "./server.php" ]

EXPOSE 8090