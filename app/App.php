<?php

namespace app;

use React\Promise\Promise;
use function React\Promise\all;

/**
 * Class App
 * @package app
 */
class App {

	private static $_instance;

	public static function getInstance() {
		return self::$_instance ?? self::$_instance = new self();
	}

	public $state;

	public function __construct() {
		$this->state = new State();
	}

	public function connectPlayer($id, $formation) {
		if ($player = $this->state->getPlayer($id)) {
			return $player;
		}
		$player = new Player($id, $formation);
		$this->state->addPlayer($player);
		/*$readyForBattle = all([
			$player->ready,
			$player->opponentReady
		]);*/
		return $player;
	}
	public function disconnectPlayer($player) {
		$this->state->removePlayer($player->id);
	}

	public function getPlayer($id) {
		return $this->state->getPlayer($id);
	}

	public function findOpponent($player) {
		$this->processOpponents();
		return $player->opponentFinded->promise();
	}


	public function processOpponents() {
		$this->findOpponentsIteration();
		$this->clearDenials();
		$this->findOpponentsIteration();
	}

	public function clearDenials() {
		foreach ($this->state->getAllPlayers() as $player) {
			if ($player->InspectingOpponentId || empty($player->DeniedOpponents)) {
				continue;
			}
			$player->DeniedOpponents = [];
		}
	}

	public function findOpponentsIteration() {
		$players = $this->state->getAllPlayers();
		$keys = array_keys($players);
		$length = count($keys);
		for ($i = 0; $i < $length; $i++) {
			for ($j = $i+1; $j < $length; $j++) {
				$first = $players[$keys[$i]];
				$second = $players[$keys[$j]];
				if ($this->checkPlayersCompatibility($first, $second)) {
					$this->bindOpponents($first, $second);
				}
			}
		}
	}

	public function bindOpponents($first, $second) {
		$first->setOpponent($second);
		$second->setOpponent($first);
		all([
			$first->ready->promise(),
			$second->ready->promise()
		])->then(function($data) use ($first, $second) {
			$this->startBattle($first, $second);
		});
	}

	public function checkPlayersCompatibility($first, $second) {
		if ($first->InspectingOpponentId || $second->InspectingOpponentId) {
			return false;
		}
		if (in_array($first->PlayerId, $second->DeniedOpponents)) {
			return false;
		}
		if (in_array($second->PlayerId, $first->DeniedOpponents)) {
			return false;
		}
		return true;
	}

	public function startBattle($firstPlayer, $secondPlayer) {
		$battle = new Battle($firstPlayer, $secondPlayer);
		$this->state->addBattle($battle);
		$firstPlayer->setBattle($battle);
		$secondPlayer->setBattle($battle);
	}
}