<?php

namespace app;

use React\Promise\Deferred;
use React\Promise\Promise;

/**
 * Class Battle
 * @package app
 */
class Battle {

	public static $lastId = 0;

	public $id;

	/**
	 * @var Player
	 */
	public $firstPlayer;
	public $secondPlayer;

	public $firstPlayerOrders;
	public $secondPlayerOrders;

	public $firstPlayerOrdersReceived;
	public $secondPlayerOrdersReceived;

	public $firstPlayerOrdersSended;
	public $secondPlayerOrdersSended;

	public $turns = [

	];

	public function __construct($firstPlayer, $secondPlayer)
	{
		$this->id = ++self::$lastId;
		$this->firstPlayer = $firstPlayer;
		$this->secondPlayer = $secondPlayer;



		$this->resetReceived();
		$this->resetSended();

		$this->firstPlayerOrdersSended->resolve();
		$this->secondPlayerOrdersSended->resolve();

	}

	public function resetReceived($player = null) {
		if (!$player || $this->firstPlayer === $player) {
			$this->firstPlayerOrdersReceived = new Deferred();
			$this->firstPlayerOrders = null;
		}
		if (!$player || $this->secondPlayer === $player) {
			$this->secondPlayerOrdersReceived = new Deferred();
			$this->secondPlayerOrders = null;
		}
	}

	public function resetSended($player = null) {
		if (!$player || $this->firstPlayer === $player) {
			$this->firstPlayerOrdersSended = new Deferred();
		}
		if (!$player || $this->secondPlayer === $player) {
			$this->secondPlayerOrdersSended = new Deferred();
		}
	}

	public function setOrders($player, $orders) {
		if ($player === $this->firstPlayer) {
			$this->firstPlayerOrders = $orders;
			$this->firstPlayerOrdersReceived->resolve($orders);
		} else {
			$this->secondPlayerOrders = $orders;
			$this->secondPlayerOrdersReceived->resolve($orders);
		}
	}

	public function getOpponentOrders($player) {
		return $player === $this->firstPlayer ? $this->secondPlayerOrders : $this->firstPlayerOrders;
	}
/*
	public function setOpponent($player) {
		$this->InspectingOpponentId = $player->PlayerId;
		$this->opponent = $player;
		$this->opponentFinded->resolve([
			'opponent' => $player
		]);
	}

	public function setReady() {
		$this->ready->resolve();
		$this->opponent->opponentReady->resolve();
	}*/


}