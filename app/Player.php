<?php

namespace app;

use React\Promise\Deferred;
use React\Promise\Promise;

/**
 * Class Player
 * @package app
 */
class Player implements \JsonSerializable {

	public $PlayerId;
	public $Formation;
	public $InspectingOpponentId;
	public $DeniedOpponents = [];

	/**
	 * @var Player
	 */
	public $opponent;

	/**
	 * @var Battle
	 */
	public $battle;

	public $opponentFinded;
	public $ready;
	public $opponentReady;
	public $battleStarted;

	public function __construct($id, $formation)
	{
		$this->PlayerId = $id;
		$this->Formation = $formation;

		$this->opponentFinded = new Deferred();
		$this->ready = new Deferred();
		$this->opponentReady = new Deferred();
		$this->battleStarted = new Deferred();

		$name = "Player[{$this->PlayerId}]";
		echo "$name connected" . PHP_EOL;
		$this->ready->promise()->then(function() use ($name) {
			echo "$name is ready" . PHP_EOL;
		});
		$this->battleStarted->promise()->then(function($data) use ($name) {
			echo "$name begins battle[{$data->id}]" . PHP_EOL;
		});
	}

	public function jsonSerialize() {
		$result = [];
		foreach (['PlayerId', 'Formation', 'InspectingOpponentId', 'DeniedOpponents'] as $key) {
			$result[$key] = $this->$key;
		}
		return $result;
	}

	public function setOpponent($player) {
		$this->InspectingOpponentId = $player->PlayerId;
		$this->opponent = $player;
		$this->opponentFinded->resolve([
			'opponent' => $player
		]);
	}

	public function setReady() {
		$this->ready->resolve();
		$this->opponent->opponentReady->resolve();
	}

	public function setBattle($battle)
	{
		$this->battle = $battle;
		$this->battleStarted->resolve($battle);
	}

}