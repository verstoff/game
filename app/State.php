<?php

namespace app;

/**
 * Class State
 * @package app
 */
class State {
	protected $_players = [];
	protected $_battles = [];

	public function addPlayer($player) {
		$this->_players[$player->PlayerId] = $player;
	}
	public function getPlayer($id) {
		return $this->_players[$id] ?? null;
	}
	public function getAllPlayers() {
		return $this->_players;
	}
	public function removePlayer($id) {
		if (isset($this->_players[$id])) {
			unset($this->_players[$id]);
		}
	}

	public function addBattle($battle) {
		$this->_battles[$battle->id] = $battle;
	}
	public function getBattle($id) {
		return $this->_battles[$id] ?? null;
	}
	public function getAllBattles() {
		return $this->_battles;
	}
	public function removeBattle($id) {
		if (isset($this->_battles[$id])) {
			unset($this->_battles[$id]);
		}
	}
}