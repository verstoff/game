<?php

namespace components;

class ConsoleTerminatingHandler {

	public static function attach($loop) {
		$pcntl = new \MKraemer\ReactPCNTL\PCNTL($loop);

		$pcntl->on(SIGTERM, function () {
			echo 'Bye'.PHP_EOL;
			die();
		});

		$pcntl->on(SIGINT, function () {
			echo 'Terminated by console'.PHP_EOL;
			die();
		});
	}
}