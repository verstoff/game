<?php
namespace components;

use React\Http\Response;
use Psr\Http\Message\ServerRequestInterface;
use React\Promise\Promise;
use React\Promise\Timer;

class Controller {

	public $request;
	public $body;
	public $loop;

	public $polling = [];
	public $pollingTimeout = 10.0;
	public $timeoutStatusCode = 408;

	public function __construct(ServerRequestInterface $request, $body, $loop)
	{
		$this->request = $request;
		$this->body = $body;
		$this->loop = $loop;
	}

	public function runAction($actionName) {
		try {
			$action = 'action' . $actionName;
			$actionResult = $this->$action();
			if (!in_array($actionName, $this->polling) || !$actionResult instanceof Promise) {
				return $actionResult;
			}
			return Timer\timeout($this->$action(), $this->pollingTimeout, $this->loop)->then(function($data) {
				return $data;
			})->otherwise(function($error) {
				if ($error instanceof Timer\TimeoutException) {
					return new Response($this->timeoutStatusCode);
				} else {
					throw $error;
				}
			});
		} catch (\Throwable $e) {
			return new Response(
				500,
				[
					'Content-Type' => 'text/plain'
				],
				$e->getMessage()
			);
		}
	}
}