<?php

namespace components;

class RequestLogger {
	public static function log($request, $data = null) {
		$params = $request->getServerParams();
		$addr = $params['REMOTE_ADDR'];
		$dateTimeObject = new \DateTime('@' . $params['REQUEST_TIME']);
		$time = $dateTimeObject->format('H:i:s');
		echo "[$time] New request from $addr" . PHP_EOL;
		echo "  Path: " . $request->getUri()->getPath() . PHP_EOL;
		echo "  Query: " . json_encode($request->getQueryParams()) . PHP_EOL;
		echo "  Body:" . PHP_EOL;
		echo $data . PHP_EOL;
	}
}