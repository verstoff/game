<?php
namespace components;

use React\Http\Response;
use Psr\Http\Message\ServerRequestInterface;
use React\Promise\Promise;

/**
 * Class Router
 * Find and instance Controller, then run action method
 * @package components
 */
class Router {

	public $routes;

	public $loop;

	public function __construct($loop)
	{
		$this->loop = $loop;
	}

	public function addRoute($path, $handler, $methods = []) {;
		$this->addRegexRoute("^$path$", $handler, $methods);
	}

	public function addRegexRoute($regex, $handler, $methods = []) {
		$regex = '/' . str_replace('/', '\/', $regex) . '/';
		$this->routes[] = new Route($regex, $handler, (array)$methods);
	}

	public function resolve(ServerRequestInterface $request, $body) {
		$method = $request->getMethod();
		$path = $request->getUri()->getPath();

		foreach ($this->routes as $route) {
			$checkResult = $this->check($route, $method, $path);
			if ($checkResult !== false) {
				$splitted = explode('.', $route->handler);
				$controllerClassName = '\controllers\\' . $splitted[0];
				$controller = new $controllerClassName($request, $body, $this->loop);
				return $controller->runAction($splitted[1]);
			}
		}

		return $this->notFound();
	}

	public function check($route, $method, $path) {
		if (!empty($route->methods) && !in_array($method, $route->methods)) {
			return false;
		}
		$matches = null;
		if (!preg_match_all($route->regex, $path, $matches)) {
			return false;
		}

		$params = [];
		foreach ($matches as $k => $v) {
			if (!is_numeric($k) && !isset($v[1])) {
				$params[$k] = $v[0];
			}
		}
		return $params;
	}

	public function notFound() {
		return new Response(404, ['Content-Type' => 'text/plain'], 'Endpoint not found');
	}
}

class Route {
	public $methods;
	public $regex;
	public $handler;

	public function __construct($regex, $handler, $methods)
	{
		$this->methods = $methods;
		$this->regex = $regex;
		$this->handler = $handler;
	}
}