<?php
namespace controllers;

use components\Controller;
use React\Http\Response;
use app\App;
use function React\Promise\some;
use function React\Promise\all;
use React\Promise\Promise;
use React\Promise\Timer;

class Battle extends Controller {

	public function actionForfeit() {
	}

	public function actionWaiting() {
		$battle = $this->getBattle();

		$app = App::getInstance();
		$player = $app->connectPlayer($id, json_decode($this->body, true));

		$opponentFinded = $app->findOpponent($player);
		return Timer\timeout($opponentFinded, $this->enterTimeout, $this->loop)->then(function($data) {
			return new Response(
				201,
				['Content-Type' => 'application/json'],
				json_encode([
					'opponent' => $data['opponent']->PlayerId,
					'formation' => $data['opponent']->Formation
				])
			);
		})->otherwise(function($error) {
			if ($error instanceof Timer\TimeoutException) {
				return new Response($this->timeoutStatusCode);
			} else {
				throw $error;
			}
		});
	}

	public function actionOrders() {
		$player = $this->getPlayer();
		$battle = $this->getBattle();



		return all([
			$battle->firstPlayerOrdersSended->promise(),
			$battle->secondPlayerOrdersSended->promise()
		])->then(function() use ($battle, $player) {
			if ($battle->firstPlayerOrders && $battle->secondPlayerOrders) {
			$battle->resetReceived();

			$battle->setOrders($player, json_decode($this->body, true));

			return all([
				$battle->firstPlayerOrdersReceived->promise(),
				$battle->secondPlayerOrdersReceived->promise()
			]);
		})->then(function() use ($battle, $player) {

			$orders = $battle->getOpponentOrders($player);
			$battle->firstPlayerOrdersSended->resolve();
			return new Response(
				201,
				['Content-Type' => 'application/json'],
				json_encode($orders)
			);
		})->then();



		$ready = all([
			$battle->firstPlayerOrdersReceived->promise(),
			$battle->secondPlayerOrdersReceived->promise()
		]);

		return $ready->then(function($data) use ($battle, $player) {
			$orders = $battle->getOpponentOrders($player);
			$battle->firstPlayerOrdersSended->resolve();
			return new Response(
				201,
				['Content-Type' => 'application/json'],
				json_encode($orders)
			);
		});


	}

	public function actionFinished() {

	}


	/**
	 * @return \app\Player | null
	 * @throws \Exception
	 */
	protected function getPlayer() {
		$id = $this->request->getQueryParams()['player_id'];
		$app = App::getInstance();
		$player = $app->getPlayer($id);
		if (!$player) {
			throw new \Exception('Player with id ' . $id . ' not found');
		}
		return $player;
	}


	/**
	 * @return \app\Battle | null
	 * @throws \Exception
	 */
	protected function getBattle() {
		$id = $this->request->getQueryParams()['battle_id'];
		$app = App::getInstance();
		$battle = $app->getBattle($id);
		if (!$battle) {
			throw new \Exception('Battle with id ' . $id . ' not found');
		}
		return $battle;
	}
}