<?php
namespace controllers;

use components\Controller;
use React\Http\Response;
use app\App;
use function React\Promise\some;
use React\Promise\Promise;
use React\Promise\Timer;

class MatchMaking extends Controller {

	public $polling = ['Enter', 'Waiting', 'Ready'];

	public function actionEnter() {
		$app = App::getInstance();
		if ($this->request->getMethod() === 'POST') {
			//$id = $this->request->getQueryParams()['id'];
			parse_str($this->body, $parsed);
			$id = $parsed['player_id'];
			$player = $app->connectPlayer($id, json_decode($parsed['player_formation'], true));
			return new Response(200);
		}

		$player = $this->getPlayer();

		return $app->findOpponent($player)->then(function($data) {
			return new Response(
				201,
				['Content-Type' => 'application/json'],
				json_encode([
					'opponent' => $data['opponent']->PlayerId,
					'formation' => $data['opponent']->Formation
				])
			);
		});
	}

	public function actionExit() {
		$player = $this->getPlayer();
		App::getInstance()->disconnectPlayer($player);
		return new Response(200);
	}

	public function actionWaiting() {
		$player = $this->getPlayer();
		$some = some([
			$player->ready->promise(),
			$player->opponentReady->promise(),
			$player->battleStarted->promise()
		], 1);
		return $some->then(function($data) {
			if ($battle = isset($data[2]) ? $data[2] : null) {
				return new Response(
					201,
					['Content-Type' => 'application/json'],
					strval($battle->id)
				);
			}
			if (array_key_exists(0, $data)) {
				return new Response(203);
			}
			if (array_key_exists(1, $data)) {
				return new Response(200);
			}
			/*if ($battle = isset($data[2]) ? $data[2] : null) {
				return new Response(
					201,
					['Content-Type' => 'application/json'],
					$battle->id
				);
			}*/
		});
	}

	public function actionReady() {
		$player = $this->getPlayer();
		$player->setReady();
		return $player->battleStarted->promise()->then(function($battle) {
			return new Response(
				201,
				['Content-Type' => 'application/json'],
				strval($battle->id)
			);
		});
	}

	public function actionChange() {

	}

	/**
	 * @return \app\Player | null
	 * @throws \Exception
	 */
	protected function getPlayer() {
		$id = $this->request->getQueryParams()['id'];
		$app = App::getInstance();
		$player = $app->getPlayer($id);
		if (!$player) {
			throw new \Exception('Player with id ' . $id . ' not found');
		}
		return $player;
	}
}