<?php
use Psr\Http\Message\ServerRequestInterface;
use React\EventLoop\Factory as LoopFactory;
use React\Http\Response;
use React\Http\Server;
use components\Router;
use React\Promise\Promise;
use components\RequestLogger;

require __DIR__ . "/vendor/autoload.php";

$config = [
	'port' => $_ENV["GAMESERVER_PORT"] ?? '8090'
];

$loop = LoopFactory::create();
\components\ConsoleTerminatingHandler::attach($loop);

// Define routing rules
$router = new Router($loop);
$router->addRoute('/matchmaking/enter', 'MatchMaking.Enter');
$router->addRoute('/matchmaking/exit', 'MatchMaking.Exit');
$router->addRoute('/matchmaking/waiting', 'MatchMaking.Waiting');
$router->addRoute('/matchmaking/ready', 'MatchMaking.Ready');
$router->addRoute('/matchmaking/change', 'MatchMaking.Change');
$router->addRoute('/battle/forfeit', 'Battle.Forfeit');
$router->addRoute('/battle/waiting', 'Battle.Waiting');
$router->addRoute('/battle/orders', 'Battle.Orders');
$router->addRoute('/battle/finished', 'Battle.Finished');

// Declare web server
$server = new Server(function (ServerRequestInterface $request) use ($router) {
	return new Promise(function ($resolve, $reject) use ($request, $router) {
		// Read body
		$data = '';
		$request->getBody()->on('data', function ($newData) use (&$data) {
			$data .= $newData;
		});

		$request->getBody()->on('end', function () use ($resolve, $request, $router, &$data){
			RequestLogger::log($request, $data);
			// Send it to router
			$resolve($router->resolve($request, $data));
		});

		// an error occures e.g. on invalid chunked encoded data or an unexpected 'end' event
		$request->getBody()->on('error', function (\Exception $exception) use ($resolve) {
			$response = new Response(
				500,
				array('Content-Type' => 'text/plain'),
				"An error occured while reading"
			);
			$resolve($response);
		});
	});
});


$socket = new \React\Socket\Server('0.0.0.0:' . $config['port'], $loop);

$server->on('error', function (Exception $e) {
	echo 'Error: ' . $e->getMessage() . PHP_EOL;
	if ($e->getPrevious() !== null) {
		$previousException = $e->getPrevious();
		echo $previousException->getMessage() . PHP_EOL;
	}
});
$server->listen($socket);

echo 'Listening on ' . str_replace('tcp:', 'http:', $socket->getAddress()) . PHP_EOL;

$loop->run();