var fetch = require('node-fetch');

var BASE_URL = 'http://mc.verstoff.ru:8090';
BASE_URL = 'http://localhost:8090';

var test = {
    request: function (url, options) {
        return fetch(BASE_URL + url, options).then(r => {
            console.log(r.url, r.status);
            return r.text().then(text => {
                console.log(text)
                r.bodyText = text;
                return r;
            });
        });
    },
    delayedRequest: function (url, options, delay) {
        return new Promise(r => {
            setTimeout(() => {
                r(this.request(url, options))
            }, delay);
        });
    },
    run: async function () {
        var enter1 = this.request('/matchmaking/enter', {
            method: 'POST',
            body: 'player_id=1&player_formation=%7B%22test%22%3A+%22test%22%7D'
        });
        var enter2 = this.delayedRequest('/matchmaking/enter', {
            method: 'POST',
            body: 'player_id=2&player_formation=%7B%22test%22%3A+%22test%22%7D'
        }, 200);

        var enteredPOST = await Promise.all([enter1, enter2]);

        var enterPolling1 = this.request('/matchmaking/enter?id=1', {
            method: 'GET'
        });
        var enterPolling2 = this.request('/matchmaking/enter?id=2', {
            method: 'GET'
        });

        var entered = await Promise.all([enterPolling1, enterPolling2]);
        var wait1 = this.request('/matchmaking/waiting?id=1');
        var wait2 = this.delayedRequest('/matchmaking/waiting?id=2', {}, 200);
        var ready1 = this.delayedRequest('/matchmaking/ready?id=1', {}, 500);
        var ready2 = this.delayedRequest('/matchmaking/ready?id=2', {}, 800);
        var battleStarted = await Promise.all([ready1, ready2]);
        var battleId = await battleStarted[0].text;


    }
}
test.run();

