var fetch = require('node-fetch');

var BASE_URL = 'http://mc.verstoff.ru:8090';
BASE_URL = 'http://localhost:8090';

var test = {
    request: function (url, options) {
        return fetch(BASE_URL + url, options).then(r => {
            console.log(r.url, r.status);
            return r.text().then(text => {
                console.log(text)
                r.bodyText = text;
                return r;
            });
        });
    },
    delayedRequest: function (url, options, delay) {
        return new Promise(r => {
            setTimeout(() => {
                r(this.request(url, options))
            }, delay);
        });
    },
    run: async function () {
        var enter1 = this.request('/matchmaking/enter', {
            method: 'POST',
            body: 'player_id=1&player_formation=%7B%22test%22%3A+%22test%22%7D'
        });
        var enterPolling1 = this.request('/matchmaking/enter?id=1', {
            method: 'GET'
        });


    }
}
test.run();

